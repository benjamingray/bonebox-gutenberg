<?php
/**
 * bonebox_base functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bonebox_base
 */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function bones_head_cleanup() {
    // category feeds
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    // post and comment feeds
    remove_action( 'wp_head', 'feed_links', 2 );
    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );
    // remove WP version from css
    add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
    // remove Wp version from scripts
    add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
} /* end bones head cleanup */

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// launching operation cleanup
add_action( 'init', 'bones_head_cleanup' );

// remove WP version from RSS
function bones_rss_version() { return ''; }
add_filter( 'the_generator', 'bones_rss_version' );



if ( ! function_exists( 'bonebox_base_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bonebox_base_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on getwid_base, use a find and replace
		 * to change 'getwid-base' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bonebox-base', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		set_post_thumbnail_size( 938 );

		//add_image_size( 'getwid_base-cropped', 938, 552, true );
		//add_image_size( 'getwid_base-large', 1130 );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'bonebox-base' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'getwid_base_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 63,
			'width'       => 63,
			'flex-width'  => true,
			'flex-height' => false,
		) );


		add_theme_support( 'editor-styles' );
		add_theme_support( 'responsive-embeds' );
		add_theme_support( 'align-wide' );
        // add bonebox_custom_fonts_url() to the array below to add another custom font, defined below.
		add_editor_style( array( '/library/css/editor-style.css', bonebox_base_fonts_url() ) );

		$default_colors = getwid_base_get_default_color_palette_colors();
		$color_palette  = apply_filters( 'getwid_base_color_palette', $default_colors );
		add_theme_support( 'editor-color-palette', $color_palette );

	}
endif;
add_action( 'after_setup_theme', 'bonebox_base_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function getwid_base_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'getwid_base_content_width', 1130 );
}

add_action( 'after_setup_theme', 'getwid_base_content_width', 0 );

function getwid_base_adjust_content_width() {
	global $content_width;

	if ( is_single() ) {
		$content_width = 748;
	}

}
add_action( 'template_redirect', 'getwid_base_adjust_content_width' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function getwid_base_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'getwid-base' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'getwid-base' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'getwid-base' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'getwid-base' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'getwid-base' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here.', 'getwid-base' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'getwid-base' ),
		'id'            => 'sidebar-4',
		'description'   => esc_html__( 'Add widgets here.', 'getwid-base' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'getwid_base_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bonebox_base_scripts() {
    $bonebox_template_directory = get_template_directory_uri();
	wp_enqueue_style( 'linearicons-free', $bonebox_template_directory . '/library/assets/linearicons/style.css', array(), '1.0.0' );

	wp_enqueue_style( 'bonebox-base-fonts', bonebox_base_fonts_url(), array(), null );

    //this style sheet must use the registration name: getwid-base-style, or the theme customizer won't work properly.
    wp_enqueue_style( 'getwid-base-style', $bonebox_template_directory. '/library/css/style.css', array(), getwid_base_get_theme_version() );

    wp_enqueue_script( 'bonebox-base-functions', $bonebox_template_directory . '/library/js/bonebox-functions.js', array('jquery'), getwid_base_get_theme_version(), true );

	wp_enqueue_script( 'getwid-base-functions', $bonebox_template_directory . '/library/js/functions.js', array( 'jquery' ), getwid_base_get_theme_version(), true );

	wp_enqueue_script( 'getwid-base-navigation', $bonebox_template_directory . '/library/js/navigation.js', array(), getwid_base_get_theme_version(), true );

	wp_enqueue_script( 'getwid-base-skip-link-focus-fix', $bonebox_template_directory . '/library/js/skip-link-focus-fix.js', array(), getwid_base_get_theme_version(), true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    //localize the main scirpt.js file to add path to the theme folder
    $themeVariables = array( 'themeURI' => $bonebox_template_directory, 'homepage' => get_bloginfo('url') );
    wp_localize_script( 'bonebox-base-functions', 'directories', $themeVariables );
}

add_action( 'wp_enqueue_scripts', 'bonebox_base_scripts' );

function bonebox_base_add_block_editor_assets() {
    $bonebox_template_directory = get_template_directory_uri();
	wp_enqueue_style( 'linearicons-free', $bonebox_template_directory . '/library/assets/linearicons/style.css', array(), '1.0.0' );
    wp_enqueue_script(
        'bonebox-block-styles-registration',
        $bonebox_template_directory . '/library/js/bonebox_block_style_registration.js',
        [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ],
        $bonebox_template_directory . '/library/js/bonebox_block_style_registration.js'
    );

}

add_action( 'enqueue_block_editor_assets', 'bonebox_base_add_block_editor_assets' );

//
function bonebox_register_block_patterns() {
    if ( function_exists( 'register_block_pattern' ) ) {
        require get_template_directory() . "/inc/bonebox-blocks/bonebox-block-patterns.php";
    }
    // Remove Default/Core Patterns
remove_theme_support( 'core-block-patterns' );
}
add_action( 'init', 'bonebox_register_block_patterns');

/**
 * Include Custom Post Types.
 */
//require get_template_directory() . '/inc/posttypes/tour-post-types.php';

/**
 * Include TGM Plugin Activation file.
 */
require get_template_directory() . '/inc/tgmpa-init.php';

/**
 * Include LinearIcons file.
 */
require get_template_directory() . '/inc/lnr-icons.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

function getwid_base_get_theme_version() {
	$theme_info = wp_get_theme( get_template() );

	return $theme_info->get( 'Version' );
}

if( ! function_exists('bonebox_custom_fonts_url' ) ) {
    /* Register a custom font */
    function bonebox_custom_fonts_url() {
        $bonebox_custom_font_url = "https://cdn.rawgit.com/mfd/f3d96ec7f0e8f034cc22ea73b3797b59/raw/856f1dbb8d807aabceb80b6d4f94b464df461b3e/gotham.css";
        return esc_url_raw( $bonebox_custom_font_url );
    }
}

if ( ! function_exists( 'bonebox_base_fonts_url' ) ) :
	/**
	 * Register Google fonts for Getwid Base theme.
	 *
	 * Create your own bonebox_base_fonts_url() function to override in a child theme.
	 *
	 * @return string Google fonts URL for the theme.
	 *
	 */
	function bonebox_base_fonts_url() {
		$fonts_url     = '';
		$font_families = array();

		/**
		 * Translators: If there are characters in your language that are not
		 * supported by Montserrat, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$font1 = esc_html_x( 'on', 'Montserrat font: on or off', 'bonebox-base' );
		if ( 'off' !== $font1 ) {
			$font_families[] = 'Montserrat: 200,400,500,700,900';
		}
		/**
		 * Translators: If there are characters in your language that are not
		 * supported by PT Serif, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$font2 = esc_html_x( 'on', 'PT Serif font: on or off', 'getwid-base' );
		if ( 'off' !== $font2 ) {
			$font_families[] = 'PT Serif: 400,400i,700,700i';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext,cyrillic' ),
		);
		if ( $font_families ) {
			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
		}

		return esc_url_raw( $fonts_url );
	}
endif;

function getwid_base_get_default_color_palette_colors() {

	$default_colors = array(
		array(
			'name'  => __( 'Primary', 'getwid-base' ),
			'slug'  => 'primary',
			'color' => '#113D34',
		),
		array(
			'name'  => __( 'Color 1', 'getwid-base' ),
			'slug'  => 'medium-green',
			'color' => '#265C50',
		),
		array(
			'name'  => __( 'Color 2', 'getwid-base' ),
			'slug'  => 'light-blue',
			'color' => '#f3f8fb',
		),
		array(
			'name'  => __( 'Color 3', 'getwid-base' ),
			'slug'  => 'blue',
			'color' => '#68c5f9',
		),
		array(
			'name'  => __( 'Color 4', 'getwid-base' ),
			'slug'  => 'gray',
			'color' => '#9ea6ac',
		),
		array(
			'name'  => __( 'Color 5', 'getwid-base' ),
			'slug'  => 'light-gray',
			'color' => '#f3f9fd',
		),
		array(
			'name'  => __( 'Color 6', 'getwid-base' ),
			'slug'  => 'dark-blue',
			'color' => '#2c3847',
		),
	);

	return $default_colors;

}

// Add Typekit library if needed
//wp_enqueue_style('typekit', '//use.typekit.net/mzq6bjg.css');

// Add jQuery Pluginss
add_action( 'wp_enqueue_scripts', 'add_jquery_plugins' );
function add_jquery_plugins() {
    
    //add_facebook_sdk script and localize it to insert the App ID
    if (function_exists('get_field') ) {
        $appID = get_field('facebook_sdk_id', 'options');
        if($appID):
            wp_register_script('add_facebook_sdk', get_template_directory_uri().'/library/js/libs/add_facebook_sdk.js', array('jquery'), '1', true);
            wp_enqueue_script('add_facebook_sdk');
            $facebookVariables = array( 'appID' => $appID );
            wp_localize_script( 'add_facebook_sdk', 'fb_variables', $facebookVariables );
        endif;
    }
}

//Register the Google API Key for ACF on the back end
function my_acf_init() {
    //get GOOGLE MAPS API KEY from options settings
    $apiKey = get_field('google_maps_api_key', 'options');
    if($apiKey) {
        acf_update_setting('google_api_key', $apiKey);
    }
}
add_action('acf/init', 'my_acf_init');

// Add an Advance Custom Fields (ACF) theme options page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

/********************************************************************
 * ALTER WORDPRESS DEFAULTS
********************************************************************/
// Add support for additional MIME types in the admin/uploads
function bonebox_custom_mime_types( $post_mime_types ) {
        //$post_mime_types['application/msword'] = array( __( 'DOCs' ), __( 'Manage DOCs' ), _n_noop( 'DOC <span class="count">(%s)</span>', 'DOC <span class="count">(%s)</span>' ) );
        //$post_mime_types['application/vnd.ms-excel'] = array( __( 'XLSs' ), __( 'Manage XLSs' ), _n_noop( 'XLS <span class="count">(%s)</span>', 'XLSs <span class="count">(%s)</span>' ) );
        $post_mime_types['application/zip'] = array( __( 'ZIPs' ), __( 'Manage ZIPs' ), _n_noop( 'ZIP <span class="count">(%s)</span>', 'ZIPs <span class="count">(%s)</span>' ) );
        
        return $post_mime_types;
}
add_filter( 'post_mime_types', 'bonebox_custom_mime_types' );

//add excerpts to pages
add_post_type_support( 'page', 'excerpt' );

// Alter the default "more" for excerpts
function new_excerpt_more( $more ) {
    return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
}
//add_filter( 'excerpt_more', 'new_excerpt_more' );

// Alter the default youTube embed code
// adds rel=0 to remove ads from the end screen
add_filter('embed_oembed_html','oembed_result', 10, 3);
function oembed_result($html, $url, $args) {
    // $args includes custom argument
    // modify $html as you need
    /* Modify video parameters. */
    if ( strstr( $html,'youtube.com/embed/' ) ) {
        $html = str_replace( '?feature=oembed', '?feature=oembed&rel=0', $html );
    }
    
    return $html;
}

// Limit number of post revisions
if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 8);

// Remove unnecessary default menu items
function bonebox_remove_menus(){
  //remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'edit.php' );                   //Posts
  //remove_menu_page( 'upload.php' );                 //Media
  //remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  //remove_menu_page( 'themes.php' );                 //Appearance
  //remove_menu_page( 'plugins.php' );                //Plugins
  //remove_menu_page( 'users.php' );                  //Users
  //remove_menu_page( 'tools.php' );                  //Tools
  //remove_menu_page( 'options-general.php' );        //Settings 
}
//add_action( 'admin_menu', 'bonebox_remove_menus' );

// change the sort order of adjacent (next/prev) post functions. The default is post date.
function my_previous_post_where() {
    global $post, $wpdb;
    return $wpdb->prepare( "WHERE p.menu_order < %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
}
add_filter( 'get_previous_post_where', 'my_previous_post_where' );

function my_next_post_where() {
    global $post, $wpdb;
    return $wpdb->prepare( "WHERE p.menu_order > %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
}
add_filter( 'get_next_post_where', 'my_next_post_where' );

function my_previous_post_sort() {
    return "ORDER BY p.menu_order desc LIMIT 1";
}
add_filter( 'get_previous_post_sort', 'my_previous_post_sort' );

function my_next_post_sort() {
    return "ORDER BY p.menu_order asc LIMIT 1";
}
add_filter( 'get_next_post_sort', 'my_next_post_sort' );
